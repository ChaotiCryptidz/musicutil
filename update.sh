#!/usr/bin/env bash

nix flake update
nix develop --command cargo install --debug cargo-upgrades
nix develop --command cargo upgrades
nix develop --command cargo update