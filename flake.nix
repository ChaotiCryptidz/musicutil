{
  description = "A tool for organising a music library";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = {
    self,
    nixpkgs,
    utils,
    ...
  }:
    {
      overlays.musicutil = final: prev: let
        system = final.system;
        pkgs = final.pkgs;
        lib = pkgs.lib;
        stdenv = pkgs.stdenv;
      in {
        musicutil = pkgs.rustPlatform.buildRustPackage rec {
          pname = "musicutil";
          version = "latest";

          src = ./.;
          cargoLock.lockFile = ./Cargo.lock;

          LIBCLANG_PATH = "${pkgs.llvmPackages.libclang.lib}/lib";

          buildFeatures = [ "taglib_extractor" ];
 
          preBuild = ''
              export BINDGEN_EXTRA_CLANG_ARGS="$(< ${stdenv.cc}/nix-support/libc-crt1-cflags) \
                $(< ${stdenv.cc}/nix-support/libc-cflags) \
                $(< ${stdenv.cc}/nix-support/cc-cflags) \
                $(< ${stdenv.cc}/nix-support/libcxx-cxxflags) \
                ${lib.optionalString stdenv.cc.isClang "-idirafter ${stdenv.cc.cc}/lib/clang/${lib.getVersion stdenv.cc.cc}/include"} \
                ${lib.optionalString stdenv.cc.isGNU "-isystem ${stdenv.cc.cc}/include/c++/${lib.getVersion stdenv.cc.cc} -isystem ${stdenv.cc.cc}/include/c++/${lib.getVersion stdenv.cc.cc}/${stdenv.hostPlatform.config} -idirafter ${stdenv.cc.cc}/lib/gcc/${stdenv.hostPlatform.config}/${lib.getVersion stdenv.cc.cc}/include"} \
            "
          '';

          postFixup = ''
            wrapProgram $out/bin/musicutil \
              --set PATH ${lib.makeBinPath [
              pkgs.ffmpeg
            ]}
          '';

          doCheck = false;
          nativeBuildInputs = with pkgs; [pkg-config rustc cargo makeWrapper];
          buildInputs = with pkgs; [ffmpeg zlib taglib];
        };
      };
      overlays.default = self.overlays.musicutil;
    }
    // utils.lib.eachSystem (utils.lib.defaultSystems) (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [self.overlays.default];
      };
    in {
      defaultPackage = self.packages."${system}".musicutil;
      packages.musicutil = pkgs.musicutil;

      apps = rec {
        musicutil = {
          type = "app";
          program = "${self.defaultPackage.${system}}/bin/musicutil";
        };
        default = musicutil;
      };

      defaultApp = self.apps."${system}".musicutil;

      devShell = pkgs.mkShell {
        RUST_SRC_PATH = pkgs.rustPlatform.rustLibSrc;
        LIBCLANG_PATH = "${pkgs.llvmPackages.libclang.lib}/lib";
        buildInputs = with pkgs; [zlib taglib pkg-config rustc cargo clippy rust-analyzer rustfmt];
        shellHook = let
          stdenv = pkgs.stdenv;
          lib = pkgs.lib;
        in ''
          export BINDGEN_EXTRA_CLANG_ARGS="$(< ${stdenv.cc}/nix-support/libc-crt1-cflags) \
            $(< ${stdenv.cc}/nix-support/libc-cflags) \
            $(< ${stdenv.cc}/nix-support/cc-cflags) \
            $(< ${stdenv.cc}/nix-support/libcxx-cxxflags) \
            ${lib.optionalString stdenv.cc.isClang "-idirafter ${stdenv.cc.cc}/lib/clang/${lib.getVersion stdenv.cc.cc}/include"} \
            ${lib.optionalString stdenv.cc.isGNU "-isystem ${stdenv.cc.cc}/include/c++/${lib.getVersion stdenv.cc.cc} -isystem ${stdenv.cc.cc}/include/c++/${lib.getVersion stdenv.cc.cc}/${stdenv.hostPlatform.config} -idirafter ${stdenv.cc.cc}/lib/gcc/${stdenv.hostPlatform.config}/${lib.getVersion stdenv.cc.cc}/include"} \
          "
        '';
      };

      lib = pkgs.musicutil.lib;
    });
}
