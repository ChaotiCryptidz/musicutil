use std::path::PathBuf;

use crate::utils::format_detection::FileFormat;

use lazy_static::lazy_static;

use super::{BoxedError, FormatHandler};

#[cfg(feature = "ffprobe_extractor")]
mod ffprobe;
#[cfg(feature = "flac_extractor")]
mod flac;
#[cfg(feature = "mp3_extractor")]
mod id3;
#[cfg(feature = "taglib_extractor")]
mod taglib;

#[cfg(not(any(
	feature = "mp3_extractor",
	feature = "flac_extractor",
	feature = "taglib_extractor",
	feature = "ffprobe_extractor"
)))]
compile_error!("at least one extractor feature must be enabled");

// For changed tags on implementations that call a command line utility to set tags
#[derive(Default)]
struct Changes {
	title: Option<String>,
	artist: Option<String>,
	album: Option<String>,
	track_number: Option<u64>,
}

impl Changes {
	#[inline]
	fn changed(&self) -> bool {
		[
			self.title.is_some(),
			self.artist.is_some(),
			self.album.is_some(),
			self.track_number.is_some(),
		]
		.contains(&true)
	}
}

type NewHandlerFuncReturn = Result<Box<dyn FormatHandler>, BoxedError>;
type NewHandlerFunc = fn(path: &PathBuf, file_format: Option<FileFormat>) -> NewHandlerFuncReturn;

pub struct Handler {
	pub supported_extensions: Vec<String>,
	pub supported_formats: Vec<FileFormat>,
	pub new: NewHandlerFunc,
}

lazy_static! {
	pub static ref SUPPORTED_EXTENSIONS: Vec<String> = {
		let mut extensions: Vec<String> = Vec::new();
		for handler in HANDLERS.iter() {
			for extension in handler.supported_extensions.iter() {
				if !extensions.contains(extension) {
					extensions.push(extension.clone());
				}
			}
		}

		extensions
	};
	pub static ref SUPPORTED_FORMATS: Vec<FileFormat> = {
		let mut formats: Vec<FileFormat> = Vec::new();
		for handler in HANDLERS.iter() {
			for format in handler.supported_formats.iter() {
				if !formats.contains(format) {
					formats.push(*format);
				}
			}
		}

		formats
	};
	pub static ref HANDLERS: Vec<Handler> = {
		let mut handlers: Vec<Handler> = Vec::new();

		#[cfg(feature = "mp3_extractor")]
		handlers.push(Handler {
			supported_extensions: vec!["mp3".to_string()],
			supported_formats: vec![FileFormat::MP3],
			new: |path, _file_format| -> NewHandlerFuncReturn {
				let handler = id3::new_handler(path)?;

				Ok(Box::from(handler))
			},
		});

		#[cfg(feature = "flac_extractor")]
		handlers.push(Handler {
			supported_extensions: vec!["flac".to_string()],
			supported_formats: vec![FileFormat::FLAC],
			new: |path, _file_format| -> NewHandlerFuncReturn {
				let handler = flac::new_handler(path)?;

				Ok(Box::from(handler))
			},
		});

		#[cfg(feature = "taglib_extractor")]
		handlers.push(Handler {
			supported_extensions: vec![
				"mp3".to_string(),
				"flac".to_string(),
				"ogg".to_string(),
				"opus".to_string(),
				"wav".to_string(),
				"wv".to_string(),
				"aiff".to_string(),
				"m4a".to_string(),
				"alac".to_string(),
			],
			supported_formats: vec![
				FileFormat::MP3,
				FileFormat::FLAC,
				FileFormat::OggVorbis,
				FileFormat::OggOpus,
				FileFormat::OggFLAC,
				FileFormat::OggSpeex,
				FileFormat::OggTheora,
				FileFormat::Wav,
				FileFormat::WavPack,
				FileFormat::AIFF,
				FileFormat::M4A,
			],
			new: |path, file_format| -> NewHandlerFuncReturn {
				let handler = taglib::new_handler(path, file_format)?;

				Ok(Box::from(handler))
			},
		});

		#[cfg(feature = "ffprobe_extractor")]
		handlers.push(Handler {
			supported_extensions: vec![
				"mp3".to_string(),
				"flac".to_string(),
				"ogg".to_string(),
				"opus".to_string(),
				"wav".to_string(),
				"wv".to_string(),
				"aiff".to_string(),
				"m4a".to_string(),
				"alac".to_string(),
			],
			supported_formats: vec![
				FileFormat::MP3,
				FileFormat::FLAC,
				FileFormat::OggVorbis,
				FileFormat::OggOpus,
				FileFormat::OggFLAC,
				FileFormat::OggSpeex,
				FileFormat::OggTheora,
				FileFormat::Wav,
				FileFormat::WavPack,
				FileFormat::AIFF,
				FileFormat::M4A,
			],
			new: |path, file_format| -> NewHandlerFuncReturn {
				let handler = ffprobe::new_handler(path, file_format)?;

				Ok(Box::from(handler))
			},
		});

		handlers
	};
}
