use std::{
	path::{Path, PathBuf},
	process::Command,
};

use crate::{
	meta,
	types::{AudioFileInfo, Tags},
	utils::format_detection::{detect_format, FileFormat},
	utils::formats::{AudioFormatError, BoxedError, FormatHandler},
};

#[cfg(feature = "replaygain")]
use replaygain::{ReplayGainData, ReplayGainRawData};

use super::Changes;

#[derive(Default)]
struct ExtractedData {
	tags: Tags,
	#[cfg(feature = "replaygain")]
	replaygain_data: Option<ReplayGainData>,
}

pub struct GenericFFMpegAudioFormat {
	file_format: FileFormat,
	path: Box<PathBuf>,

	extracted_data: ExtractedData,
	changes: Changes,
}

impl GenericFFMpegAudioFormat {
	fn analyze(&mut self) -> Result<(), BoxedError> {
		let output = ffprobe::analyze(&self.path, Some(meta::FFPROBE), false);

		if let Err(err) = output {
			return Err(Box::from(err));
		}

		let output = output.unwrap();

		self.extracted_data.tags = Tags {
			title: output.tags.title,
			artist: output.tags.artist,
			album: output.tags.album,
			track_number: output.tags.track_number,
		};

		#[cfg(feature = "replaygain")]
		if output.tags.replaygain_track_gain.is_some()
			&& output.tags.replaygain_track_peak.is_some()
		{
			self.extracted_data.replaygain_data = Some(ReplayGainData {
				track_gain: output.tags.replaygain_track_gain.unwrap(),
				track_peak: output.tags.replaygain_track_peak.unwrap(),
			});
		} else {
			self.extracted_data.replaygain_data = None;
		}

		Ok(())
	}
}

impl FormatHandler for GenericFFMpegAudioFormat {
	fn get_tags(&self, allow_missing: bool) -> Result<Tags, BoxedError> {
		let mut tags = self.extracted_data.tags.clone();

		if let Some(title) = &self.changes.title {
			tags.title = title.clone();
		}

		if let Some(artist) = &self.changes.artist {
			tags.artist = artist.clone();
		}

		if let Some(album) = &self.changes.album {
			tags.album = Some(album.clone());
		}

		if !allow_missing {
			if tags.title.is_empty() {
				return Err(Box::new(AudioFormatError::MissingTitle));
			}
			if tags.artist.is_empty() {
				return Err(Box::new(AudioFormatError::MissingArtist));
			}
		}

		Ok(tags)
	}

	#[cfg(feature = "replaygain")]
	fn contains_replaygain_tags(&self) -> bool {
		false
	}

	#[cfg(feature = "replaygain")]
	fn supports_replaygain(&self) -> bool {
		false
	}

	fn set_title(&mut self, title: String) -> Result<(), BoxedError> {
		self.changes.title = Some(title);
		Ok(())
	}

	fn set_artist(&mut self, artist: String) -> Result<(), BoxedError> {
		self.changes.artist = Some(artist);
		Ok(())
	}

	fn set_album(&mut self, album: String) -> Result<(), BoxedError> {
		self.changes.album = Some(album);
		Ok(())
	}

	#[cfg(feature = "replaygain")]
	fn set_replaygain_data(&mut self, _data: ReplayGainRawData) -> Result<(), BoxedError> {
		panic!("ffprobe doesn't support setting replaygain data, check supports_replaygain()")
	}

	fn save_changes(&mut self) -> Result<(), BoxedError> {
		if !self.changes.changed() {
			return Ok(());
		}

		let mut args: Vec<String> = Vec::new();

		let tempdir = tempfile::tempdir()?;
		let temp_file = tempdir
			.path()
			.join(PathBuf::from(self.path.file_name().unwrap()));

		args.extend(vec![
			"-i".to_string(),
			self.path.to_string_lossy().to_string(),
		]);

		args.extend(vec!["-c".to_string(), "copy".to_string()]);

		if let Some(title) = &self.changes.title {
			args.extend(vec![
				"-metadata".to_string(),
				format!("title=\"{}\"", title.as_str()),
			])
		}

		if let Some(artist) = &self.changes.artist {
			args.extend(vec![
				"-metadata".to_string(),
				format!("artist={}", artist.as_str()),
			])
		}

		if let Some(album) = &self.changes.album {
			args.extend(vec![
				"-metadata".to_string(),
				format!("album={}", album.as_str()),
			])
		}

		args.push(temp_file.to_string_lossy().to_string());

		let output = Command::new(crate::meta::FFMPEG).args(args).output()?;

		println!("{:?}", String::from_utf8(output.stderr));

		std::fs::copy(temp_file, self.path.to_path_buf())?;

		Ok(())
	}

	fn get_audio_file_info(
		&mut self,
		allow_missing_tags: bool,
	) -> Result<AudioFileInfo, BoxedError> {
		Ok(AudioFileInfo {
			tags: self.get_tags(allow_missing_tags)?,
			#[cfg(feature = "replaygain")]
			contains_replaygain: self.contains_replaygain_tags(),
			#[cfg(feature = "replaygain")]
			supports_replaygain: self.supports_replaygain(),
			format: Some(self.file_format),
		})
	}
}

pub fn new_handler(
	path: &Path,
	file_format: Option<FileFormat>,
) -> Result<GenericFFMpegAudioFormat, BoxedError> {
	let mut file_format = file_format;
	if file_format.is_none() {
		file_format = Some(detect_format(path)?);
	}

	let mut handler = GenericFFMpegAudioFormat {
		file_format: file_format.unwrap(),
		path: Box::new(path.to_path_buf()),
		extracted_data: ExtractedData::default(),
		changes: Changes::default(),
	};

	handler.analyze()?;

	Ok(handler)
}
