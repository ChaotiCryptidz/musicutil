use std::path::PathBuf;

use id3::TagLike;

use crate::{
	types::{AudioFileInfo, Tags},
	utils::format_detection::FileFormat,
	utils::formats::{AudioFormatError, BoxedError, FormatHandler},
};

#[cfg(feature = "replaygain")]
use replaygain::ReplayGainRawData;

pub struct ID3AudioFormat {
	id3_tags: id3::Tag,
	path: Box<PathBuf>,
}

impl FormatHandler for ID3AudioFormat {
	fn get_tags(&self, allow_missing: bool) -> Result<Tags, BoxedError> {
		let title = self.id3_tags.title();
		let artist = self.id3_tags.artist();
		let album = self.id3_tags.album();

		if !allow_missing {
			if title.is_none() {
				return Err(Box::new(AudioFormatError::MissingTitle));
			}
			if artist.is_none() {
				return Err(Box::new(AudioFormatError::MissingArtist));
			}
		}

		Ok(Tags {
			title: String::from(title.unwrap()),
			artist: String::from(artist.unwrap()),
			album: album.map(|f| f.to_string()),
			track_number: self
				.id3_tags
				.track()
				.map(|track_number| track_number as u64),
		})
	}

	#[cfg(feature = "replaygain")]
	fn contains_replaygain_tags(&self) -> bool {
		let frames = self.id3_tags.frames();

		let mut contains_replaygain_tags = false;

		for frame in frames {
			if frame.id() == "TXXX" {
				if let Some(extended_text) = frame.content().extended_text() {
					match extended_text.description.as_str() {
						"REPLAYGAIN_TRACK_GAIN" => {
							contains_replaygain_tags = true;
						}
						"REPLAYGAIN_TRACK_PEAK" => {
							contains_replaygain_tags = true;
						}
						_ => {}
					}
				}
			}
		}

		contains_replaygain_tags
	}

	#[cfg(feature = "replaygain")]
	fn supports_replaygain(&self) -> bool {
		true
	}

	fn set_title(&mut self, title: String) -> Result<(), BoxedError> {
		self.id3_tags.set_title(title);

		Ok(())
	}

	fn set_artist(&mut self, artist: String) -> Result<(), BoxedError> {
		self.id3_tags.set_artist(artist);

		Ok(())
	}

	fn set_album(&mut self, album: String) -> Result<(), BoxedError> {
		self.id3_tags.set_album(album);

		Ok(())
	}

	#[cfg(feature = "replaygain")]
	fn set_replaygain_data(&mut self, data: ReplayGainRawData) -> Result<(), BoxedError> {
		let frames = self.id3_tags.remove("TXXX");

		for frame in frames {
			if let Some(extended_text) = frame.content().extended_text() {
				if extended_text.description.starts_with("REPLAYGAIN") {
					continue;
				}
			}
			self.id3_tags.add_frame(frame);
		}

		self.id3_tags.add_frame(id3::Frame::with_content(
			"TXXX",
			id3::Content::ExtendedText(id3::frame::ExtendedText {
				description: "REPLAYGAIN_TRACK_GAIN".to_string(),
				value: format!("{:.2} dB", data.track_gain),
			}),
		));

		self.id3_tags.add_frame(id3::Frame::with_content(
			"TXXX",
			id3::Content::ExtendedText(id3::frame::ExtendedText {
				description: "REPLAYGAIN_TRACK_PEAK".to_string(),
				value: format!("{:.6}", data.track_peak),
			}),
		));

		Ok(())
	}

	fn save_changes(&mut self) -> Result<(), BoxedError> {
		self.id3_tags
			.write_to_path(self.path.as_path(), id3::Version::Id3v24)?;
		Ok(())
	}

	fn get_audio_file_info(
		&mut self,
		allow_missing_tags: bool,
	) -> Result<AudioFileInfo, BoxedError> {
		Ok(AudioFileInfo {
			tags: self.get_tags(allow_missing_tags)?,
			format: Some(FileFormat::MP3),
			#[cfg(feature = "replaygain")]
			supports_replaygain: self.supports_replaygain(),
			#[cfg(feature = "replaygain")]
			contains_replaygain: self.contains_replaygain_tags(),
		})
	}
}

pub fn new_handler(path: &PathBuf) -> Result<ID3AudioFormat, BoxedError> {
	let id3_tags = id3::Tag::read_from_path(path)?;

	Ok(ID3AudioFormat {
		id3_tags,
		path: Box::new(path.clone()),
	})
}
