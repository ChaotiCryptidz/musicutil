use std::path::Path;

use taglib::{
	new_taglib_file,
	traits::{File, Tag},
	TagLibFileType,
};

use crate::{
	types::{AudioFileInfo, Tags},
	utils::format_detection::FileFormat,
	utils::formats::{AudioFormatError, BoxedError, FormatHandler},
};

#[cfg(feature = "replaygain")]
use replaygain::ReplayGainRawData;

pub struct TaglibAudioFormat {
	file: taglib::TagLibFile,
	file_format: Option<FileFormat>,
}

impl FormatHandler for TaglibAudioFormat {
	fn get_tags(&self, allow_missing: bool) -> Result<Tags, BoxedError> {
		let tags = self.file.tag()?;

		let mut title = tags.title();
		if title.is_none() {
			if !allow_missing {
				return Err(Box::new(AudioFormatError::MissingTitle));
			} else {
				title = Some("".to_string())
			}
		}

		let mut artist = tags.artist();
		if artist.is_none() {
			if !allow_missing {
				return Err(Box::new(AudioFormatError::MissingArtist));
			} else {
				artist = Some("".to_string())
			}
		}

		Ok(Tags {
			title: title.unwrap(),
			artist: artist.unwrap(),
			album: tags.album(),
			track_number: tags.track(),
		})
	}

	#[cfg(feature = "replaygain")]
	fn contains_replaygain_tags(&self) -> bool {
		if let Some(format) = self.file_format {
			if format == FileFormat::OggOpus {
				let oggtag = self.file.oggtag().expect("oggtag not available?");

				return oggtag.get_field("R128_TRACK_GAIN".to_string()).is_some();
			}
			match format {
				FileFormat::OggVorbis | FileFormat::OggFLAC | FileFormat::OggSpeex => {
					let oggtag = self.file.oggtag().expect("oggtag not available?");
					let gain = oggtag.get_field("REPLAYGAIN_TRACK_GAIN".to_string());
					let peak = oggtag.get_field("REPLAYGAIN_TRACK_PEAK".to_string());
					return gain.is_some() && peak.is_some();
				}
				_ => {}
			}
		}

		false
	}

	#[cfg(feature = "replaygain")]
	fn supports_replaygain(&self) -> bool {
		if let Some(format) = self.file_format {
			return matches!(
				format,
				// All Ogg formats support ReplayGain
				FileFormat::OggVorbis
					| FileFormat::OggOpus
					| FileFormat::OggFLAC
					| FileFormat::OggSpeex
			);
			// Both "support" ReplayGain but not implemented yet
			// FileFormat::Wav |
			// FileFormat::WavPack
		}

		false
	}

	fn set_title(&mut self, title: String) -> Result<(), BoxedError> {
		let mut tags = self.file.tag()?;
		tags.set_title(title);

		Ok(())
	}

	fn set_artist(&mut self, artist: String) -> Result<(), BoxedError> {
		let mut tags = self.file.tag()?;
		tags.set_artist(artist);

		Ok(())
	}

	fn set_album(&mut self, album: String) -> Result<(), BoxedError> {
		let mut tags = self.file.tag()?;
		tags.set_album(album);

		Ok(())
	}

	#[cfg(feature = "replaygain")]
	fn set_replaygain_data(&mut self, data: ReplayGainRawData) -> Result<(), BoxedError> {
		if let Some(format) = self.file_format {
			match format {
				FileFormat::OggOpus
				| FileFormat::OggVorbis
				| FileFormat::OggFLAC
				| FileFormat::OggSpeex => {
					let oggtag = self.file.oggtag().expect("oggtag not available?");

					if format == FileFormat::OggOpus {
						let data = data.to_normal(true);

						oggtag.add_field("R128_TRACK_GAIN".to_string(), data.track_gain);
					} else {
						let data = data.to_normal(false);

						oggtag.add_field("REPLAYGAIN_TRACK_GAIN".to_string(), data.track_gain);
						oggtag.add_field("REPLAYGAIN_TRACK_PEAK".to_string(), data.track_peak);
					}
				}
				_ => {
					panic!("setting replaygain not supported on this format");
				}
			}
		}

		Ok(())
	}

	fn save_changes(&mut self) -> Result<(), BoxedError> {
		let res = self.file.save();

		match res {
			Ok(()) => Ok(()),
			Err(e) => Err(Box::new(e)),
		}
	}

	fn get_audio_file_info(
		&mut self,
		allow_missing_tags: bool,
	) -> Result<AudioFileInfo, BoxedError> {
		Ok(AudioFileInfo {
			tags: self.get_tags(allow_missing_tags)?,
			#[cfg(feature = "replaygain")]
			contains_replaygain: self.contains_replaygain_tags(),
			#[cfg(feature = "replaygain")]
			supports_replaygain: self.supports_replaygain(),
			format: self.file_format,
		})
	}
}

pub fn new_handler(
	path: &Path,
	file_format: Option<FileFormat>,
) -> Result<TaglibAudioFormat, BoxedError> {
	let mut taglib_format: Option<TagLibFileType> = None;
	if let Some(format) = file_format {
		taglib_format = match format {
			FileFormat::OggVorbis => Some(TagLibFileType::OggVorbis),
			FileFormat::OggOpus => Some(TagLibFileType::OggOpus),
			FileFormat::OggFLAC => Some(TagLibFileType::OggFLAC),
			FileFormat::OggSpeex => Some(TagLibFileType::OggSpeex),
			FileFormat::M4A => Some(TagLibFileType::MP4),
			_ => None,
		}
	}

	Ok(TaglibAudioFormat {
		file_format,
		file: new_taglib_file(path.to_string_lossy().to_string(), taglib_format)?,
	})
}
