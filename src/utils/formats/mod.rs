mod handlers;

use std::error::Error;
use std::path::Path;

use relative_file::traits::RelativeFileTrait;
use thiserror::Error;

use super::format_detection::detect_format;
use crate::types::{AudioFile, AudioFileInfo, Tags};

#[cfg(feature = "replaygain")]
use replaygain::ReplayGainRawData;

type BoxedError = Box<dyn Error>;

pub trait FormatHandler {
	fn get_tags(&self, allow_missing: bool) -> Result<Tags, BoxedError>;
	#[cfg(feature = "replaygain")]
	fn contains_replaygain_tags(&self) -> bool;
	#[cfg(feature = "replaygain")]
	fn supports_replaygain(&self) -> bool;

	fn set_title(&mut self, title: String) -> Result<(), BoxedError>;
	fn set_artist(&mut self, artist: String) -> Result<(), BoxedError>;
	fn set_album(&mut self, artist: String) -> Result<(), BoxedError>;
	#[cfg(feature = "replaygain")]
	fn set_replaygain_data(&mut self, data: ReplayGainRawData) -> Result<(), BoxedError>;

	fn save_changes(&mut self) -> Result<(), BoxedError>;

	fn get_audio_file_info(
		&mut self,
		allow_missing_tags: bool,
	) -> Result<AudioFileInfo, BoxedError>;
}

#[derive(Error, Debug)]
pub enum AudioFormatError {
	#[error("title missing from tags")]
	MissingTitle,
	#[error("artist missing from tags")]
	MissingArtist,
}

pub fn get_format_handler(file: &AudioFile) -> Result<Box<dyn FormatHandler>, BoxedError> {
	let format = detect_format(&file.join_path_to())?;
	let path = file.join_path_to();

	for handler in handlers::HANDLERS.iter() {
		if !handler
			.supported_extensions
			.contains(&file.extension().unwrap())
		{
			continue;
		}

		if !handler.supported_formats.contains(&format) {
			continue;
		}

		let new = handler.new;

		return new(&path, Some(format));
	}

	panic!("no supported handler found");
}

fn is_supported_extension(ext: &str) -> bool {
	handlers::SUPPORTED_EXTENSIONS.contains(&ext.to_string())
}

pub fn is_supported_file(file_path: &Path) -> bool {
	let ext = file_path.extension();

	if ext.is_none() {
		return false;
	}

	let ext = ext.unwrap().to_str().unwrap();

	if !is_supported_extension(ext) {
		return false;
	}

	let format = detect_format(file_path);
	if format.is_err() {
		return false;
	}

	let format = format.unwrap();

	handlers::SUPPORTED_FORMATS.contains(&format)
}
