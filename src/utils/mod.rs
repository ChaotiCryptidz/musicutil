pub mod format_detection;
pub mod transcoder;

pub mod formats;
mod music_scanner;

pub use formats::is_supported_file;
pub use music_scanner::scan_for_music;
