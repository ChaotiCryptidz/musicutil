use std::{fs, path::PathBuf};

use crate::types::{AudioFile, FolderMeta};
use relative_file::{traits::RelativeFileTrait, RelativeFile};
use walkdir::WalkDir;

use super::is_supported_file;

pub fn find_extra_files(
	src_dir: String,
	file: &AudioFile,
) -> Result<Vec<RelativeFile>, Box<dyn std::error::Error>> {
	let mut extra_files: Vec<RelativeFile> = Vec::new();

	for entry in fs::read_dir(file.path_to())? {
		let entry = entry?;
		if !entry.metadata()?.is_file() {
			continue;
		}
		let entry_path = entry.path();

		let extension = entry_path.extension();
		if extension.is_none() {
			continue;
		}

		if entry_path.file_stem().unwrap().to_string_lossy() == file.filename()
			&& extension.map(|x| x.to_string_lossy().to_string()) != file.extension()
		{
			extra_files.push(RelativeFile::from_path(src_dir.clone(), entry_path.clone()));
		}
	}

	Ok(extra_files)
}

pub fn scan_for_music(src_dir: &String) -> Result<Vec<AudioFile>, Box<dyn std::error::Error>> {
	let mut files: Vec<AudioFile> = Vec::new();

	for entry in WalkDir::new(src_dir) {
		let entry = entry.unwrap();
		let entry_path = entry.into_path();

		if entry_path.is_dir() {
			continue;
		}

		if is_supported_file(&entry_path) {
			let mut file = AudioFile::from_path(src_dir.clone(), entry_path.clone());

			let mut folder_meta_path = PathBuf::from(entry_path.parent().unwrap());
			folder_meta_path.push("folder.meta");

			if folder_meta_path.exists() {
				file.folder_meta = FolderMeta::load(&folder_meta_path)?;
			}

			file.extra_files
				.extend(find_extra_files(src_dir.clone(), &file)?);

			files.push(file);
		}
	}

	Ok(files)
}
