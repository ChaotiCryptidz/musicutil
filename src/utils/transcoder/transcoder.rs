use std::{fs, process::Command, sync::mpsc::Sender, thread::JoinHandle};

use crate::types::AudioFile;
use relative_file::traits::RelativeFileTrait;
use string_error::static_err;

use super::{progress_monitor, types::TranscodeConfig};

pub fn transcode(
	file: AudioFile,
	dest: String,
	config: &TranscodeConfig,
	progress_sender: Option<Sender<String>>,
) -> Result<(), Box<dyn std::error::Error>> {
	let mut command_args: Vec<String> = Vec::new();
	command_args.extend(vec!["-y".to_string(), "-hide_banner".to_string()]);

	command_args.extend(vec![
		"-i".to_string(),
		file.join_path_to().to_string_lossy().to_string(),
	]);

	if let Some(encoder) = &config.encoder {
		command_args.extend(vec!["-c:a".to_string(), encoder.to_string()]);
	}

	if let Some(container) = &config.container {
		command_args.extend(vec!["-f".to_string(), container.to_string()]);
	}

	if let Some(sample_rate) = &config.sample_rate {
		command_args.extend(vec!["-ar".to_string(), sample_rate.to_string()]);
	}

	if let Some(channels) = &config.channels {
		command_args.extend(vec!["-ac".to_string(), channels.to_string()]);
	}

	if let Some(quality) = &config.quality {
		command_args.extend(vec!["-q:a".to_string(), quality.to_string()]);
	}

	if let Some(bitrate) = &config.bitrate {
		command_args.extend(vec!["-b:a".to_string(), bitrate.to_string()]);
	}

	if let Some(album_art_codec) = &config.album_art_codec {
		command_args.extend(vec!["-c:v".to_string(), album_art_codec.to_string()]);

		if config.album_art_height.is_some() && config.album_art_width.is_some() {
			let mut height: i16 = config.album_art_height.unwrap() as i16;
			if height == 0 {
				height = -1;
			}

			let mut width = config.album_art_width.unwrap() as i16;
			if width == 0 {
				width = -1;
			}

			command_args.extend(vec![
				"-vf".to_string(),
				format!("scale={}:{}", width, height),
			]);
		}
	}

	command_args.push(dest);

	let mut progress_thread: Option<JoinHandle<()>> = None;
	let mut progress_file: Option<String> = None;

	if let Some(sender) = &progress_sender {
		let result = progress_monitor(file.join_path_to(), sender);

		if let Ok(result) = result {
			progress_thread = Some(result.1);
			progress_file = Some(result.0.clone());
			command_args.extend(vec![
				"-progress".to_string(),
				result.0,
				"-nostats".to_string(),
			]);
		}
	}

	let output = Command::new(crate::meta::FFMPEG)
		.args(command_args)
		.output()
		.expect("failed to execute process");

	if let Some(sender) = progress_sender {
		drop(sender);
	}

	if let Some(thread) = progress_thread {
		let _ = fs::remove_file(progress_file.unwrap());
		thread.join().expect("thread couldn't join");
	}

	if !output.status.success() {
		print!("{}", String::from_utf8(output.stderr).unwrap());
		return Err(static_err("FFmpeg Crashed"));
	}

	Ok(())
}
