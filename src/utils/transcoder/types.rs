use std::{fs::File, io::BufReader, path::PathBuf};

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Preset {
	pub name: String,
	pub config: TranscodeConfig,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PresetCategory {
	pub name: String,
	pub presets: Vec<Preset>,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct TranscodeConfig {
	pub encoder: Option<String>,
	pub file_extension: Option<String>,
	pub container: Option<String>,
	pub bitrate: Option<String>,
	pub quality: Option<String>,
	pub sample_rate: Option<String>,
	pub channels: Option<String>,
	pub album_art_codec: Option<String>,
	pub album_art_height: Option<u16>,
	pub album_art_width: Option<u16>,
}

impl TranscodeConfig {
	pub fn load(path: String) -> Result<TranscodeConfig, Box<dyn std::error::Error>> {
		let path_buf = PathBuf::from(&path);
		let extension = path_buf.extension().and_then(std::ffi::OsStr::to_str);
		let file = File::open(path)?;
		let reader = BufReader::new(file);

		match extension {
			Some("json") => {
				let conf: TranscodeConfig =
					serde_json::from_reader(reader).expect("error while reading json");
				Ok(conf)
			}
			Some("yml") | Some("yaml") | Some(&_) | None => {
				let conf: TranscodeConfig =
					serde_json::from_reader(reader).expect("error while reading json");
				Ok(conf)
			}
		}
	}
}
