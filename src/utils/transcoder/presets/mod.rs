mod flac;
mod g726;
mod mp3;
mod opus;
mod speex;
mod vorbis;
mod wav;

use lazy_static::lazy_static;

use crate::utils::transcoder::types::PresetCategory;

use super::types::TranscodeConfig;

lazy_static! {
	#[derive(Debug)]
	pub static ref TRANSCODE_CONFIGS: Vec<PresetCategory> = {
		let mut preset_categories: Vec<PresetCategory> = Vec::new();

		mp3::add_presets(&mut preset_categories);
		opus::add_presets(&mut preset_categories);
		vorbis::add_presets(&mut preset_categories);
		g726::add_presets(&mut preset_categories);
		speex::add_presets(&mut preset_categories);
		flac::add_presets(&mut preset_categories);
		wav::add_presets(&mut preset_categories);

		preset_categories
	};
}

pub fn print_presets() {
	for category in TRANSCODE_CONFIGS.iter() {
		println!("Category {}:", category.name);
		for preset in category.presets.iter() {
			println!("- {}", preset.name)
		}
	}
}

pub fn get_preset(name: String) -> Option<TranscodeConfig> {
	for category in TRANSCODE_CONFIGS.iter() {
		for preset in category.presets.iter() {
			if preset.name == name {
				return Some(preset.config.clone());
			}
		}
	}

	None
}

#[derive(Debug)]
pub enum PresetError {
	PresetNonExistant,
	NoneSpecified,
	LoadError(Box<dyn std::error::Error>),
}

impl std::fmt::Display for PresetError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			PresetError::PresetNonExistant => write!(f, "Preset does not exist"),
			PresetError::NoneSpecified => write!(f, "No preset or config specified"),
			PresetError::LoadError(err) => write!(f, "Error loading config: {}", err),
		}
	}
}

impl std::error::Error for PresetError {}

pub fn transcode_preset_or_config(
	preset_name: Option<&String>,
	config_path: Option<&String>,
) -> Result<TranscodeConfig, PresetError> {
	if let Some(preset_name) = preset_name {
		let preset_config = get_preset(preset_name.to_string());

		match preset_config {
			Some(config) => Ok(config),
			None => Err(PresetError::PresetNonExistant),
		}
	} else if let Some(config_path) = config_path {
		return match TranscodeConfig::load(config_path.to_string()) {
			Ok(config) => Ok(config),
			Err(e) => Err(PresetError::LoadError(e)),
		};
	} else {
		Err(PresetError::NoneSpecified)
	}
}
