use crate::utils::transcoder::types::Preset;
use crate::utils::transcoder::types::PresetCategory;
use crate::utils::transcoder::types::TranscodeConfig;

pub fn add_presets(preset_categories: &mut Vec<PresetCategory>) {
	let mut presets: Vec<Preset> = Vec::new();
	for bitrate in [
		8, 16, 24, 32, 40, 48, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320,
	] {
		presets.push(Preset {
			name: format!("mp3-{}k", bitrate).to_string(),
			config: TranscodeConfig {
				file_extension: Some("mp3".to_string()),
				encoder: Some("libmp3lame".to_string()),
				container: Some("mp3".to_string()),
				bitrate: Some(format!("{}k", bitrate).to_string()),
				..TranscodeConfig::default()
			},
		})
	}

	for quality in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] {
		presets.push(Preset {
			name: format!("mp3-v{}", quality).to_string(),
			config: TranscodeConfig {
				file_extension: Some("mp3".to_string()),
				encoder: Some("libmp3lame".to_string()),
				container: Some("mp3".to_string()),
				quality: Some(format!("{}", quality).to_string()),
				..TranscodeConfig::default()
			},
		})
	}

	preset_categories.push(PresetCategory {
		name: "mp3".to_string(),
		presets,
	});
}
