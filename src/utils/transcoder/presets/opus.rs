use crate::utils::transcoder::types::Preset;
use crate::utils::transcoder::types::PresetCategory;
use crate::utils::transcoder::types::TranscodeConfig;

pub fn add_presets(preset_categories: &mut Vec<PresetCategory>) {
	let mut presets: Vec<Preset> = Vec::new();
	for bitrate in [16, 24, 32, 64, 96, 128, 256] {
		presets.push(Preset {
			name: format!("opus-{}k", bitrate).to_string(),
			config: TranscodeConfig {
				file_extension: Some("opus".to_string()),
				encoder: Some("libopus".to_string()),
				container: Some("oga".to_string()),
				bitrate: Some(format!("{}k", bitrate).to_string()),
				..TranscodeConfig::default()
			},
		})
	}

	preset_categories.push(PresetCategory {
		name: "opus".to_string(),
		presets,
	});
}
