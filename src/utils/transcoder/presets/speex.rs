use crate::utils::transcoder::types::Preset;
use crate::utils::transcoder::types::PresetCategory;
use crate::utils::transcoder::types::TranscodeConfig;

pub fn add_presets(preset_categories: &mut Vec<PresetCategory>) {
	let mut presets: Vec<Preset> = Vec::new();
	for quality in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] {
		presets.push(Preset {
			name: format!("speex-q{}", quality).to_string(),
			config: TranscodeConfig {
				file_extension: Some("ogg".to_string()),
				encoder: Some("libspeex".to_string()),
				container: Some("ogg".to_string()),
				quality: Some(format!("{}", quality).to_string()),
				..TranscodeConfig::default()
			},
		})
	}

	preset_categories.push(PresetCategory {
		name: "speex".to_string(),
		presets,
	});
}
