use crate::utils::transcoder::types::Preset;
use crate::utils::transcoder::types::PresetCategory;
use crate::utils::transcoder::types::TranscodeConfig;

pub fn add_presets(preset_categories: &mut Vec<PresetCategory>) {
	let mut presets: Vec<Preset> = Vec::new();
	for bitrate in [16, 24, 32, 64, 96, 128, 256] {
		presets.push(Preset {
			name: format!("g726-{}k", bitrate).to_string(),
			config: TranscodeConfig {
				file_extension: Some("mka".to_string()),
				encoder: Some("g726".to_string()),
				container: Some("matroska".to_string()),
				sample_rate: Some("8000".to_string()),
				channels: Some("1".to_string()),
				bitrate: Some(format!("{}k", bitrate).to_string()),
				album_art_codec: Some("png".to_string()),
				..TranscodeConfig::default()
			},
		})
	}

	preset_categories.push(PresetCategory {
		name: "g726".to_string(),
		presets,
	});
}
