use crate::utils::transcoder::types::Preset;
use crate::utils::transcoder::types::PresetCategory;
use crate::utils::transcoder::types::TranscodeConfig;

pub fn add_presets(preset_categories: &mut Vec<PresetCategory>) {
	preset_categories.push(PresetCategory {
		name: "flac".to_string(),
		presets: Vec::from([Preset {
			name: "flac".to_string(),
			config: TranscodeConfig {
				encoder: Some("flac".to_string()),
				container: Some("flac".to_string()),
				file_extension: Some("flac".to_string()),
				..TranscodeConfig::default()
			},
		}]),
	})
}
