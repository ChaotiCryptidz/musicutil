use crate::utils::transcoder::types::Preset;
use crate::utils::transcoder::types::PresetCategory;
use crate::utils::transcoder::types::TranscodeConfig;

pub fn add_presets(preset_categories: &mut Vec<PresetCategory>) {
	preset_categories.push(PresetCategory {
		name: "wav".to_string(),
		presets: Vec::from([Preset {
			name: "wav".to_string(),
			config: TranscodeConfig {
				container: Some("wav".to_string()),
				file_extension: Some("wav".to_string()),
				..TranscodeConfig::default()
			},
		}]),
	})
}
