use std::{
	fs,
	io::{BufRead, BufReader, Seek, SeekFrom},
	path::PathBuf,
	sync::mpsc::{self, Sender},
	thread::{self, JoinHandle},
	time::Duration,
};

use notify::{EventKind, RecommendedWatcher, RecursiveMode, Watcher};

fn get_file_length_milliseconds(path: PathBuf) -> Result<u64, Box<dyn std::error::Error>> {
	match ffprobe::analyze(&path, Some(crate::meta::FFPROBE), true) {
		Ok(data) => Ok((data.duration * 1000.0).round() as u64),
		Err(e) => Err(Box::from(e)),
	}
}

pub fn progress_monitor(
	source_filepath: PathBuf,
	sender_base: &Sender<String>,
) -> Result<(String, JoinHandle<()>), Box<dyn std::error::Error>> {
	let total_length_millis = get_file_length_milliseconds(source_filepath)?;

	let tempdir = tempfile::tempdir()?;
	let file_path = tempdir.path().join("progress.log");
	let file_path_string = file_path.to_str().unwrap().to_string();
	fs::File::create(&file_path)?;

	let sender = sender_base.clone();
	let child = thread::spawn(move || {
		let _ = &tempdir;

		let (tx, rx) = mpsc::channel();
		let mut watcher = RecommendedWatcher::new(
			tx,
			notify::Config::default().with_poll_interval(Duration::from_millis(100)),
		)
		.expect("could not watch for ffmpeg log progress status");

		watcher
			.watch(&file_path, RecursiveMode::NonRecursive)
			.unwrap();

		let mut pos = 0;

		'outer: for res in rx {
			if res.is_err() {
				break 'outer;
			}

			let res = res.unwrap();

			match res.kind {
				EventKind::Modify(_) => {
					let file = fs::File::open(&file_path);
					let mut file = match file {
						Ok(f) => f,
						Err(_e) => {
							break 'outer;
						}
					};
					file.seek(SeekFrom::Start(pos)).unwrap();

					pos = file.metadata().unwrap().len();

					let reader = BufReader::new(file);
					for line in reader.lines() {
						let ln = line.unwrap();

						if ln == "progress=end" {
							break 'outer;
						}

						// there is a out_time_ms however it seems to be the same value as out_time_us
						// https://trac.ffmpeg.org/ticket/7345
						// it has not yet been actually fixed
						if ln.starts_with("out_time_us=") {
							let out_time = ln.strip_prefix("out_time_us=").unwrap().to_string();
							let out_time_ms = match out_time.parse::<u64>() {
								Ok(ts) => ((ts as f64) / 1000.0).round(),
								Err(_) => 0.0,
							};
							if sender
								.send(format!(
									"{:.2}%",
									((out_time_ms / total_length_millis as f64) * 100.0)
								))
								.is_err()
							{
								break 'outer;
							};
						}
					}
				}
				EventKind::Remove(_) => break 'outer,
				_ => {}
			}
		}
	});

	Ok((file_path_string, child))
}
