pub mod args;
pub mod commands;
pub mod meta;
pub mod types;
pub mod utils;

use args::{CLIArgs, Commands};
use clap::Parser;

fn main() -> Result<(), Box<dyn std::error::Error>> {
	let cli = CLIArgs::parse();

	let command = cli.command.to_owned();

	match command {
		Commands::Process(subcommand_args) => {
			commands::process::process_command(cli, &subcommand_args)
		}
		#[cfg(feature = "command_genhtml")]
		Commands::Genhtml(subcommand_args) => commands::genhtml::genhtml_command(cli, &subcommand_args),
		Commands::Transcode(subcommand_args) => {
			commands::transcode::transcode_command(cli, &subcommand_args)
		}
		Commands::Copy(subcommand_args) => commands::copy::copy_command(cli, &subcommand_args),
		Commands::Tags(subcommand_args) => commands::tags::tags_command(cli, &subcommand_args),
		Commands::Presets(subcommand_args) => {
			commands::presets::presets_command(cli, &subcommand_args)
		}
	}
}
