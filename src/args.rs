use clap::{Parser, Subcommand};

use crate::commands;

#[derive(Debug, Parser)]
#[clap()]
pub struct CLIArgs {
	#[clap(subcommand)]
	pub command: Commands,
}

#[derive(Debug, Clone, Subcommand)]
pub enum Commands {
	Process(commands::process::ProcessCommandArgs),
	#[cfg(feature = "command_genhtml")]
	Genhtml(commands::genhtml::GenHTMLCommandArgs),
	Transcode(commands::transcode::TranscodeCommandArgs),
	Copy(commands::copy::CopyCommandArgs),
	Tags(commands::tags::TagsArgs),
	Presets(commands::presets::PresetsArgs),
}
