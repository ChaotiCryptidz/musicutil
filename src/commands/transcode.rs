use std::path::PathBuf;
use std::process::exit;
use std::sync::mpsc;
use std::thread;
use std::thread::JoinHandle;

use relative_file::traits::RelativeFileTrait;

use crate::args::CLIArgs;
use crate::types::AudioFile;
use crate::utils::transcoder::presets::print_presets;
use crate::utils::transcoder::presets::transcode_preset_or_config;
use crate::utils::transcoder::transcode;

#[derive(Debug, Clone, clap::Args)]
pub struct TranscodeCommandArgs {
	pub source: String,
	pub dest: String,
	#[clap(long)]
	pub preset: Option<String>,
	#[clap(long)]
	pub transcode_config: Option<String>,
	#[clap(long)]
	pub ignore_extension: bool,
	#[clap(long)]
	pub hide_progress: bool,
}

pub fn transcode_command(
	_args: CLIArgs,
	transcode_args: &TranscodeCommandArgs,
) -> Result<(), Box<dyn std::error::Error>> {
	let input_file = AudioFile::from_path("".to_string(), PathBuf::from(&transcode_args.source));
	let output_file = AudioFile::from_path("".to_string(), PathBuf::from(&transcode_args.dest));

	let mut preset = transcode_args.preset.clone().unwrap_or("auto".to_string());

	if preset == "list" {
		print_presets();
		exit(0);
	}

	if preset == "auto" && transcode_args.transcode_config.is_none() {
		let extension = output_file
			.extension()
			.clone()
			.expect("a extension is required for the output file");

		preset = match extension.as_str() {
			"mp3" => "mp3-v2",
			"opus" => "opus-96k",
			"flac" => "flac",
			"ogg" => "vorbis-v6",
			_ => {
				println!(
					"could not auto determine a suitable transcode preset for extension '{}'",
					output_file.extension().unwrap()
				);
				exit(0);
			}
		}
		.to_string();
	}

	let transcode_config = transcode_preset_or_config(
		match transcode_args.transcode_config {
			Some(_) => None,
			None => Some(&preset),
		},
		transcode_args.transcode_config.as_ref(),
	)
	.expect("transcode config error");

	println!("Transcoding");

	if !transcode_args.ignore_extension {
		if let Some(ref file_extension) = transcode_config.file_extension {
			if Some(file_extension) != output_file.extension().as_ref() {
				panic!(
					concat!(
						"{} is not the recommended ",
						"extension for specified transcode config ",
						"please change it to {} ",
						"or run with --ignore-extension"
					),
					output_file.extension().unwrap(),
					file_extension
				);
			}
		}
	}

	let (tx, rx) = mpsc::channel::<String>();
	let mut child: Option<JoinHandle<()>> = None;

	if !transcode_args.hide_progress {
		child = Some(thread::spawn(move || loop {
			let progress = rx.recv();

			if let Ok(progress_str) = progress {
				println!("Transcode Progress: {}", progress_str);
			} else {
				break;
			}
		}));
	}

	transcode(
		input_file,
		transcode_args.dest.clone(),
		&transcode_config,
		match transcode_args.hide_progress {
			true => None,
			false => Some(tx),
		},
	)?;

	if let Some(child) = child {
		child.join().expect("oops! the child thread panicked");
	}

	println!("Transcode Finished");

	Ok(())
}
