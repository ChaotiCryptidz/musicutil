use crate::args::CLIArgs;

#[derive(Debug, Clone, clap::Args)]
pub struct PresetsArgs {
	#[clap(subcommand)]
	pub command: Option<PresetsCommands>,
}

#[derive(Debug, Clone, clap::Subcommand)]
pub enum PresetsCommands {
	Print,
	Dump(PresetsDumpCommandArgs),
}

#[derive(Debug, Clone, clap::Args)]
pub struct PresetsDumpCommandArgs {
	pub preset: String,
	#[clap(long, value_enum, default_value_t=DumpFormat::YAML)]
	pub format: DumpFormat,
}

#[derive(Debug, Clone, clap::ValueEnum)]
pub enum DumpFormat {
	YAML,
	JSON,
}

impl Default for DumpFormat {
	fn default() -> Self {
		Self::YAML
	}
}

pub fn presets_command(
	_args: CLIArgs,
	presets_args: &PresetsArgs,
) -> Result<(), Box<dyn std::error::Error>> {
	let command = presets_args.command.to_owned();
	match command {
		None | Some(PresetsCommands::Print) => crate::utils::transcoder::presets::print_presets(),
		Some(PresetsCommands::Dump(x)) => {
			let preset = match crate::utils::transcoder::presets::get_preset(x.preset) {
				Some(preset) => preset,
				None => {
					println!("invalid preset specified");
					std::process::exit(1);
				}
			};

			match x.format {
				DumpFormat::YAML => {
					println!("{}", serde_yaml::to_string(&preset)?)
				}
				DumpFormat::JSON => {
					println!("{}", serde_json::to_string_pretty(&preset)?)
				}
			}
		}
	}

	Ok(())
}
