use std::collections::HashMap;
use std::path::PathBuf;

use relative_file::traits::RelativeFileTrait;
use serde::Serialize;

use crate::args::CLIArgs;
use crate::types::AudioFile;
use crate::utils::formats::get_format_handler;

#[derive(Debug, Clone, clap::Args)]
pub struct GetTagsCommandArgs {
	pub files: Vec<String>,
	#[clap(long)]
	pub json: bool,
}

#[derive(Debug, Clone, Serialize)]
struct Tags {
	title: String,
	artist: String,
	album: Option<String>,
	track_number: Option<u64>,
}

fn from_main_tags(tags: &crate::types::Tags) -> Tags {
	Tags {
		title: tags.title.clone(),
		artist: tags.artist.clone(),
		album: tags.album.clone(),
		track_number: tags.track_number,
	}
}

pub fn get_tags_command(
	_args: CLIArgs,
	get_tags_args: &GetTagsCommandArgs,
) -> Result<(), Box<dyn std::error::Error>> {
	let mut files: Vec<AudioFile> = Vec::new();

	for file in get_tags_args.files.iter() {
		files.push(AudioFile::from_path("".to_string(), PathBuf::from(file)));
	}

	for file in files.iter_mut() {
		let handler = get_format_handler(file)?;

		file.info.tags = handler.get_tags(true)?;
	}

	if files.len() == 1 {
		let file = files.first().unwrap();

		if get_tags_args.json {
			println!(
				"{}",
				serde_json::to_string_pretty(&from_main_tags(&file.info.tags)).unwrap()
			);
		} else {
			println!("{:#?}", from_main_tags(&file.info.tags));
		}
	} else if get_tags_args.json {
		let mut result: HashMap<String, Tags> = HashMap::new();
		for file in files.iter() {
			result.insert(
				file.join_path_to().to_string_lossy().to_string(),
				from_main_tags(&file.info.tags),
			);
		}
		println!("{}", serde_json::to_string_pretty(&result)?);
	} else {
		for file in files.iter() {
			println!(
				"{}: {:#?}",
				file.join_path_to().to_string_lossy(),
				from_main_tags(&file.info.tags)
			);
		}
	}

	Ok(())
}
