use std::path::PathBuf;

use crate::args::CLIArgs;
use crate::types::AudioFile;
use crate::utils::formats::get_format_handler;

#[derive(Debug, Clone, clap::Args)]
pub struct SetTagsCommandArgs {
	pub files: Vec<String>,
	#[clap(long)]
	pub title: Option<String>,
	#[clap(long)]
	pub artist: Option<String>,
	#[clap(long)]
	pub album: Option<String>,
}

pub fn set_tags_command(
	_args: CLIArgs,
	add_tags_args: &SetTagsCommandArgs,
) -> Result<(), Box<dyn std::error::Error>> {
	let mut files: Vec<AudioFile> = Vec::new();

	for file in add_tags_args.files.iter() {
		files.push(AudioFile::from_path("".to_string(), PathBuf::from(file)));
	}

	for file in files.iter() {
		let mut handler = get_format_handler(file)?;

		if let Some(title) = &add_tags_args.title {
			handler.set_title(title.clone())?;
		}

		if let Some(artist) = &add_tags_args.artist {
			handler.set_artist(artist.clone())?;
		}

		if let Some(album) = &add_tags_args.album {
			handler.set_album(album.clone())?;
		}

		handler.save_changes()?;
	}

	Ok(())
}
