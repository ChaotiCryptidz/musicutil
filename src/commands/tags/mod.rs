use crate::args::CLIArgs;

mod get;
mod set;

#[derive(Debug, Clone, clap::Args)]
pub struct TagsArgs {
	#[clap(subcommand)]
	pub command: TagsCommands,
}

#[derive(Debug, Clone, clap::Subcommand)]
pub enum TagsCommands {
	Get(get::GetTagsCommandArgs),
	Set(set::SetTagsCommandArgs),
}

pub fn tags_command(args: CLIArgs, tags_args: &TagsArgs) -> Result<(), Box<dyn std::error::Error>> {
	let command = tags_args.command.to_owned();
	match command {
		TagsCommands::Get(subcommand_args) => get::get_tags_command(args, &subcommand_args),
		TagsCommands::Set(subcommand_args) => set::set_tags_command(args, &subcommand_args),
	}
}
