use std::path::PathBuf;

use relative_file::RelativeFile;
use serde::Deserialize;

use crate::utils::format_detection::FileFormat;

#[derive(Default, Debug, Clone)]
pub struct Tags {
	pub title: String,
	pub artist: String,
	pub album: Option<String>,
	pub track_number: Option<u64>,
}

#[derive(Default, Debug, Clone)]
pub struct AudioFileInfo {
	pub tags: Tags,
	#[cfg(feature = "replaygain")]
	pub contains_replaygain: bool,
	#[cfg(feature = "replaygain")]
	pub supports_replaygain: bool,
	pub format: Option<FileFormat>,
}

#[derive(Debug, Clone)]
pub struct AudioFile {
	file_base: RelativeFile,

	pub extra_files: Vec<RelativeFile>,

	pub info: AudioFileInfo,

	pub folder_meta: FolderMeta,
}

impl AudioFile {
	pub fn from_path(source_dir: String, full_path: PathBuf) -> AudioFile {
		let file_base = RelativeFile::from_path(source_dir, full_path);

		AudioFile {
			file_base,
			extra_files: Vec::new(),
			info: AudioFileInfo::default(),
			folder_meta: FolderMeta::default(),
		}
	}

	pub fn as_relative_file(&self) -> &RelativeFile {
		&self.file_base
	}
}

impl relative_file::traits::RelativeFileTrait for AudioFile {
	fn filename(&self) -> String {
		self.file_base.filename()
	}

	fn extension(&self) -> Option<String> {
		self.file_base.extension()
	}

	fn path_to(&self) -> PathBuf {
		self.file_base.path_to()
	}

	fn path_from_source(&self) -> PathBuf {
		self.file_base.path_from_source()
	}

	fn join_filename(&self) -> String {
		self.file_base.join_filename()
	}

	fn join_path_to(&self) -> PathBuf {
		self.file_base.join_path_to()
	}

	fn join_path_from_source(&self) -> PathBuf {
		self.file_base.join_path_from_source()
	}

	fn change_filename(&mut self, filename: String) {
		self.file_base.change_filename(filename)
	}
}

#[derive(Debug, Clone, Deserialize, Default)]
pub struct FolderMeta {
	pub is_album: bool,
}

impl FolderMeta {
	pub fn load(path: &PathBuf) -> Result<FolderMeta, Box<dyn std::error::Error>> {
		let file = std::fs::File::open(path)?;
		let reader = std::io::BufReader::new(file);

		let u: FolderMeta =
			serde_yaml::from_reader(reader).expect("error while reading folder meta");
		Ok(u)
	}
}
