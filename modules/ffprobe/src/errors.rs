use std::{fmt, io, process};

#[derive(Debug)]
pub enum AnalyzeError {
	FFProbeError(FFProbeError),
	IOError(io::Error),
	ParseError(serde_json::Error),
	TagsMissing(),
}

impl std::error::Error for AnalyzeError {}

impl fmt::Display for AnalyzeError {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			AnalyzeError::FFProbeError(err) => write!(f, "{}", err),
			AnalyzeError::IOError(err) => write!(f, "{}", err),
			AnalyzeError::ParseError(err) => write!(f, "{}", err),
			AnalyzeError::TagsMissing() => write!(f, "Tags could not be found"),
		}
	}
}

#[derive(Debug, Clone)]
pub struct FFProbeError {
	pub exit_status: process::ExitStatus,
	pub stderr: String,
}

impl fmt::Display for FFProbeError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(
			f,
			"ffprobe exited with error code {}, stderr: {}",
			self.exit_status.code().unwrap(),
			self.stderr
		)
	}
}
