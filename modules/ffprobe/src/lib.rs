pub mod errors;
mod ffprobe_output;
pub mod types;

use std::{path::Path, process::Command};

use crate::ffprobe_output::FFProbeOutputTags;

use self::errors::{AnalyzeError, FFProbeError};

fn extract(
	path: &Path,
	ffprobe_command: Option<&str>,
) -> Result<ffprobe_output::FFProbeOutput, AnalyzeError> {
	let output = Command::new(ffprobe_command.unwrap_or("ffprobe"))
		.args([
			"-v",
			"quiet",
			"-print_format",
			"json",
			"-show_format",
			"-show_streams",
			path.to_str().unwrap(),
		])
		.output();

	if let Err(err) = output {
		return Err(AnalyzeError::IOError(err));
	}

	let output = output.unwrap();

	if !output.status.success() {
		return Err(AnalyzeError::FFProbeError(FFProbeError {
			exit_status: output.status,
			stderr: String::from_utf8(output.stderr).unwrap(),
		}));
	}

	let output = String::from_utf8(output.stdout).unwrap();
	let ffprobe_out: serde_json::Result<ffprobe_output::FFProbeOutput> =
		serde_json::from_str(output.as_str());

	Ok(ffprobe_out.unwrap())
}

pub fn analyze(
	path: &Path,
	ffprobe_command: Option<&str>,
	ignore_missing_tags: bool,
) -> Result<types::FFProbeData, AnalyzeError> {
	let raw_data = extract(path, ffprobe_command)?;

	let tags = match raw_data.format.tags {
		Some(tags) => tags,
		None => {
			let mut found_tags: Option<FFProbeOutputTags> = None;
			for stream in raw_data.streams.iter() {
				if let Some(tags) = &stream.tags {
					found_tags = Some(tags.clone());
					break;
				}
			}

			match found_tags {
				Some(tags) => tags,
				None => {
					if !ignore_missing_tags {
						return Err(AnalyzeError::TagsMissing());
					}
					FFProbeOutputTags::default()
				}
			}
		}
	};

	let mut data = types::FFProbeData {
		tags: tags.into(),
		duration: match raw_data.format.duration.parse::<f64>() {
			Ok(ts) => ts,
			Err(_) => 0.0,
		},
		album_art: None,
	};

	for stream in raw_data.streams.into_iter() {
		if stream.codec_type == "video" {
			data.album_art = Some(types::FFProbeAlbumArt {
				codec_name: stream.codec_name,
				height: stream.height.unwrap(),
				width: stream.width.unwrap(),
			});
		}
	}

	Ok(data)
}
