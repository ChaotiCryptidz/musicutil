use serde::Serialize;

use super::ffprobe_output;

#[derive(Default, Debug, Clone, Serialize)]
pub struct FFProbeTags {
	pub title: String,
	pub artist: String,
	pub album: Option<String>,
	pub replaygain_track_peak: Option<String>,
	pub replaygain_track_gain: Option<String>,
	pub track_number: Option<u64>,
}

impl From<ffprobe_output::FFProbeOutputTags> for FFProbeTags {
	fn from(val: ffprobe_output::FFProbeOutputTags) -> Self {
		let mut track_number: Option<u64> = None;
		if let Some(number) = val.track_number {
			let mut number = number.clone();
			if number.contains('/') {
				let mut split = number.split('/');
				number = split.next().unwrap().to_string()
			}

			track_number = match number.parse::<u64>() {
				Ok(n) => Some(n),
				Err(_e) => None,
			}
		}

		FFProbeTags {
			title: val.title,
			artist: val.artist,
			album: val.album,
			replaygain_track_peak: val.replaygain_track_peak,
			replaygain_track_gain: val.replaygain_track_gain,
			track_number,
		}
	}
}

#[derive(Debug, Clone, Serialize)]
pub struct FFProbeAlbumArt {
	pub codec_name: String,
	pub height: u16,
	pub width: u16,
}

#[derive(Debug, Clone, Serialize)]
pub struct FFProbeData {
	// in seconds
	pub duration: f64,
	pub tags: FFProbeTags,
	pub album_art: Option<FFProbeAlbumArt>,
}
