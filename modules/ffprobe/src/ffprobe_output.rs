use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct FFProbeOutput {
	pub format: FFProbeOutputFormat,
	pub streams: Vec<FFProbeOutputStream>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct FFProbeOutputStream {
	pub index: u8,
	pub channels: Option<u8>,
	pub duration: String,
	pub codec_name: String,
	pub codec_type: String,
	pub height: Option<u16>,
	pub width: Option<u16>,
	pub tags: Option<FFProbeOutputTags>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct FFProbeOutputFormat {
	// Some formats don't have tags in the output format but do in the Streams
	pub tags: Option<FFProbeOutputTags>,
	pub duration: String,
}

#[derive(Default, Debug, Clone, Deserialize)]
pub struct FFProbeOutputTags {
	#[serde(alias = "TITLE")]
	pub title: String,
	#[serde(default, alias = "ARTIST")]
	pub artist: String,
	#[serde(default, alias = "ALBUM")]
	pub album: Option<String>,
	#[serde(alias = "TRACK", alias = "track")]
	pub track_number: Option<String>,

	#[serde(default, alias = "REPLAYGAIN_TRACK_PEAK")]
	pub replaygain_track_peak: Option<String>,
	#[serde(default, alias = "REPLAYGAIN_TRACK_GAIN")]
	pub replaygain_track_gain: Option<String>,
}
