use std::path::PathBuf;

pub mod traits {
	use std::path::PathBuf;

	pub trait RelativeFileTrait {
		fn filename(&self) -> String;
		fn change_filename(&mut self, filename: String);

		fn extension(&self) -> Option<String>;
		fn path_to(&self) -> PathBuf;
		fn path_from_source(&self) -> PathBuf;

		fn join_filename(&self) -> String;
		fn join_path_to(&self) -> PathBuf;
		fn join_path_from_source(&self) -> PathBuf;
	}
}

#[derive(Debug, Clone)]
pub struct RelativeFile {
	filename: String,
	extension: Option<String>,

	// relative path to file's folder
	path_to: PathBuf,
	// path to folder from source
	path_from_source: PathBuf,
}

impl RelativeFile {
	pub fn from_path(source_dir: String, full_path: PathBuf) -> crate::RelativeFile {
		let full_file_path = PathBuf::from(&source_dir).join(full_path);

		let filename_without_extension = full_file_path
			.file_stem()
			.expect("filename is invalid")
			.to_string_lossy()
			.to_string();

		let extension = full_file_path.extension();

		let extension = if let Some(extension) = extension {
			Some(extension.to_string_lossy().to_string())
		} else {
			None
		};

		let path_from_src = full_file_path
			.strip_prefix(&source_dir)
			.expect("couldn't get path relative to source");

		let mut folder_path_from_src = path_from_src.to_path_buf();
		folder_path_from_src.pop();

		let path_to = PathBuf::from(&source_dir).join(&folder_path_from_src);

		RelativeFile {
			filename: filename_without_extension,
			extension,
			path_from_source: folder_path_from_src,
			path_to,
		}
	}
}

impl traits::RelativeFileTrait for RelativeFile {
	fn join_filename(&self) -> String {
		return match &self.extension {
			Some(extension) => format!("{}.{}", self.filename, extension),
			None => self.filename.clone(),
		};
	}

	fn join_path_to(&self) -> PathBuf {
		PathBuf::from(&self.path_to).join(self.join_filename())
	}
	fn join_path_from_source(&self) -> PathBuf {
		PathBuf::from(&self.path_from_source).join(self.join_filename())
	}

	fn path_to(&self) -> PathBuf {
		self.path_to.clone()
	}

	fn path_from_source(&self) -> PathBuf {
		self.path_from_source.clone()
	}

	fn filename(&self) -> String {
		self.filename.clone()
	}

	fn extension(&self) -> Option<String> {
		self.extension.clone()
	}

	fn change_filename(&mut self, filename: String) {
		self.filename = filename
	}
}

#[cfg(test)]
mod tests {
	use crate::traits::RelativeFileTrait;
	use std::path::PathBuf;

	#[test]
	fn filename() {
		let file =
			crate::RelativeFile::from_path("source/".to_string(), PathBuf::from("path/file.txt"));
		assert_eq!(file.join_filename(), "file.txt")
	}

	#[test]
	fn change_filename() {
		let mut file =
			crate::RelativeFile::from_path("source/".to_string(), PathBuf::from("path/file.txt"));
		file.change_filename("new_file".to_string());
		assert_eq!(file.join_filename(), "new_file.txt")
	}

	#[test]
	fn missing_extension() {
		let file =
			crate::RelativeFile::from_path("source/".to_string(), PathBuf::from("path/file"));
		assert_eq!(file.join_filename(), "file");
		assert_eq!(file.extension(), None);
	}

	#[test]
	fn paths() {
		let file = crate::RelativeFile::from_path(
			"source/".to_string(),
			PathBuf::from("path/to/file.txt"),
		);
		assert_eq!(file.path_to(), PathBuf::from("source/path/to"));
		assert_eq!(file.path_from_source(), PathBuf::from("path/to"));
		assert_eq!(file.extension(), Some("txt".to_string()));
	}
}
