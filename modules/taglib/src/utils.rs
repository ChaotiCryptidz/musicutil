use std::{ffi::CStr, os::raw::c_char};

pub fn c_str_to_str(c_str: *const c_char) -> Option<String> {
	if c_str.is_null() {
		None
	} else {
		let bytes = unsafe { CStr::from_ptr(c_str).to_bytes() };

		if bytes.is_empty() {
			None
		} else {
			Some(String::from_utf8_lossy(bytes).to_string())
		}
	}
}
