use crate::errors::TagLibError;

pub trait File {
	fn save(&mut self) -> Result<(), TagLibError>;
}

pub trait Tag {
	fn title(&self) -> Option<String>;
	fn artist(&self) -> Option<String>;
	fn album(&self) -> Option<String>;
	fn track(&self) -> Option<u64>;
	fn set_title(&mut self, title: String);
	fn set_artist(&mut self, artist: String);
	fn set_album(&mut self, album: String);
}
