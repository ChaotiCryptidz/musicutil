#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void* TagLib_File;
typedef void* TagLib_Tag;

TagLib_File *wrap_taglib_file_new(const char *filename);
TagLib_File *wrap_taglib_file_new_with_type(const char *filename, short taglib_type);

TagLib_Tag *wrap_taglib_file_tag(TagLib_File *file);
void wrap_taglib_file_free(TagLib_File *file);
bool wrap_taglib_file_save(TagLib_File *file);

char* wrap_taglib_tag_title(TagLib_Tag *tag);
char* wrap_taglib_tag_artist(TagLib_Tag *tag);
char* wrap_taglib_tag_album(TagLib_Tag *tag);
unsigned int wrap_taglib_tag_track(TagLib_Tag *tag);
void wrap_taglib_tag_set_title(TagLib_Tag *tag, const char *title);
void wrap_taglib_tag_set_artist(TagLib_Tag *tag, const char *artist);
void wrap_taglib_tag_set_album(TagLib_Tag *tag, const char *album);

void wrap_taglib_opustag_add_field(TagLib_Tag *tag, const char *key, const char *value);
void wrap_taglib_opustag_remove_fields(TagLib_Tag *tag, const char *key);
char* wrap_taglib_opustag_get_field(TagLib_Tag *tag, const char *key);


#ifdef __cplusplus
}
#endif