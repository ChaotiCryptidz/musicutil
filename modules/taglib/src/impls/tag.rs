use std::ffi::CString;

use crate::{bindings, traits::Tag, utils::c_str_to_str};

pub struct TagLibTag {
	pub ctx: *mut bindings::TagLib_Tag,
}

impl Tag for TagLibTag {
	fn title(&self) -> Option<String> {
		let title_ref = unsafe { bindings::wrap_taglib_tag_title(self.ctx) };

		c_str_to_str(title_ref)
	}
	fn set_title(&mut self, title: String) {
		let title = CString::new(title).unwrap();

		unsafe { bindings::wrap_taglib_tag_set_title(self.ctx, title.as_ptr()) };
	}
	fn artist(&self) -> Option<String> {
		let artist_ref = unsafe { bindings::wrap_taglib_tag_artist(self.ctx) };

		c_str_to_str(artist_ref)
	}
	fn set_artist(&mut self, artist: String) {
		let artist = CString::new(artist).unwrap();

		unsafe { bindings::wrap_taglib_tag_set_artist(self.ctx, artist.as_ptr()) };
	}
	fn album(&self) -> Option<String> {
		let album_ref = unsafe { bindings::wrap_taglib_tag_album(self.ctx) };

		c_str_to_str(album_ref)
	}
	fn set_album(&mut self, album: String) {
		let album = CString::new(album).unwrap();

		unsafe { bindings::wrap_taglib_tag_set_album(self.ctx, album.as_ptr()) };
	}
	fn track(&self) -> Option<u64> {
		let track = unsafe { bindings::wrap_taglib_tag_track(self.ctx) };

		if track == 0 {
			None
		} else {
			Some(track as u64)
		}
	}
}
