use std::ffi::CString;

use crate::{bindings, errors::TagLibError, traits::File, TagLibFileType};

use super::{oggtag::TagLibOggTag, tag::TagLibTag};

pub struct TagLibFile {
	ctx: *mut bindings::TagLib_File,
	taglib_type: Option<TagLibFileType>,
}

pub fn new_taglib_file(
	filepath: String,
	taglib_type: Option<TagLibFileType>,
) -> Result<TagLibFile, TagLibError> {
	let filename_c = CString::new(filepath).unwrap();
	let filename_c_ptr = filename_c.as_ptr();

	let file = unsafe {
		if let Some(taglib_type) = taglib_type {
			bindings::wrap_taglib_file_new_with_type(filename_c_ptr, (taglib_type as u8).into())
		} else {
			bindings::wrap_taglib_file_new(filename_c_ptr)
		}
	};

	if file.is_null() {
		return Err(TagLibError::InvalidFile);
	}

	Ok(TagLibFile {
		ctx: file,
		taglib_type,
	})
}

impl TagLibFile {
	pub fn tag(&self) -> Result<TagLibTag, TagLibError> {
		let tag = unsafe { bindings::wrap_taglib_file_tag(self.ctx) };
		if tag.is_null() {
			return Err(TagLibError::MetadataUnavailable);
		}

		Ok(TagLibTag { ctx: tag })
	}

	pub fn oggtag(&self) -> Result<TagLibOggTag, TagLibError> {
		if let Some(taglib_type) = &self.taglib_type {
			let supported = matches!(
				taglib_type,
				TagLibFileType::OggFLAC
					| TagLibFileType::OggOpus
					| TagLibFileType::OggSpeex
					| TagLibFileType::OggVorbis
			);

			if !supported {
				panic!("ogg tag not supported")
			}
		}

		let tag = unsafe { bindings::wrap_taglib_file_tag(self.ctx) };
		if tag.is_null() {
			return Err(TagLibError::MetadataUnavailable);
		}

		Ok(TagLibOggTag { ctx: tag })
	}
}

impl File for TagLibFile {
	fn save(&mut self) -> Result<(), TagLibError> {
		let result = unsafe { bindings::wrap_taglib_file_save(self.ctx) };

		match result {
			true => Ok(()),
			false => Err(TagLibError::SaveError),
		}
	}
}

impl Drop for TagLibFile {
	fn drop(&mut self) {
		unsafe {
			bindings::wrap_taglib_file_free(self.ctx);
		}
	}
}
