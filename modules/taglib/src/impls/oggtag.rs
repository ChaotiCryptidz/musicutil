use std::ffi::CString;

use crate::{bindings, utils::c_str_to_str};

pub struct TagLibOggTag {
	pub ctx: *mut bindings::TagLib_Tag,
}

impl TagLibOggTag {
	pub fn get_field(&self, key: String) -> Option<String> {
		let key = CString::new(key).unwrap();
		let value = unsafe { bindings::wrap_taglib_opustag_get_field(self.ctx, key.as_ptr()) };

		if value.is_null() {
			None
		} else {
			c_str_to_str(value)
		}
	}

	pub fn add_field(&self, key: String, value: String) {
		let key = CString::new(key).unwrap();
		let value = CString::new(value).unwrap();

		unsafe { bindings::wrap_taglib_opustag_add_field(self.ctx, key.as_ptr(), value.as_ptr()) };
	}

	pub fn remove_fields(&self, key: String) {
		let key = CString::new(key).unwrap();

		unsafe { bindings::wrap_taglib_opustag_remove_fields(self.ctx, key.as_ptr()) };
	}
}
