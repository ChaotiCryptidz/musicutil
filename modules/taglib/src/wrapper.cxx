#include "wrapper.h"

#include <taglib/fileref.h>
#include <taglib/tfile.h>
#include <taglib/vorbisfile.h>
#include <taglib/oggflacfile.h>
#include <taglib/opusfile.h>
#include <taglib/mp4file.h>
#include <taglib/speexfile.h>

#include <string.h>

char *stringToCharArray(const TagLib::String &s) {
  const std::string str = s.to8Bit(true);
  return strdup(str.c_str());
}

TagLib::String charArrayToString(const char *s) {
  return TagLib::String(s, TagLib::String::UTF8);
}

void wrap_taglib_free(void* pointer) {
  free(pointer);
}

TagLib_File *wrap_taglib_file_new(const char *filename) {
  return reinterpret_cast<TagLib_File *>(TagLib::FileRef::create(filename));
}

TagLib_File *wrap_taglib_file_new_with_type(const char *filename, short taglib_type) {
  if (taglib_type == 1) {
    return reinterpret_cast<TagLib_File *>(new TagLib::Ogg::FLAC::File(filename));
  }
  if (taglib_type == 2) {
    return reinterpret_cast<TagLib_File *>(new TagLib::Ogg::Opus::File(filename));
  }
  if (taglib_type == 3) {
    return reinterpret_cast<TagLib_File *>(new TagLib::Ogg::Speex::File(filename));
  }
  if (taglib_type == 4) {
    return reinterpret_cast<TagLib_File *>(new TagLib::Ogg::Vorbis::File(filename));
  }
  if (taglib_type == 5) {
    return reinterpret_cast<TagLib_File *>(new TagLib::MP4::File(filename));
  }
  return reinterpret_cast<TagLib_File *>(TagLib::FileRef::create(filename));
}


void wrap_taglib_file_free(TagLib_File *file) {
  delete reinterpret_cast<TagLib::File *>(file);
}

bool wrap_taglib_file_save(TagLib_File *file) {
  return reinterpret_cast<TagLib::File *>(file)->save();
}

TagLib_Tag *wrap_taglib_file_tag(TagLib_File *file) {
  const TagLib::File *f = reinterpret_cast<const TagLib::File *>(file);
  return reinterpret_cast<TagLib_Tag *>(f->tag());
}

char* wrap_taglib_tag_title(TagLib_Tag *tag) {
  const TagLib::Tag *t = reinterpret_cast<const TagLib::Tag *>(tag);
  return stringToCharArray(t->title());
}

char* wrap_taglib_tag_artist(TagLib_Tag *tag) {
  const TagLib::Tag *t = reinterpret_cast<const TagLib::Tag *>(tag);
  return stringToCharArray(t->artist());
}

char* wrap_taglib_tag_album(TagLib_Tag *tag) {
  const TagLib::Tag *t = reinterpret_cast<const TagLib::Tag *>(tag);
  return stringToCharArray(t->album());
}

unsigned int wrap_taglib_tag_track(TagLib_Tag *tag) {
  const TagLib::Tag *t = reinterpret_cast<const TagLib::Tag *>(tag);
  return t->track();
}

void wrap_taglib_tag_set_title(TagLib_Tag *tag, const char *title) {
  TagLib::Tag *t = reinterpret_cast<TagLib::Tag *>(tag);
  t->setTitle(charArrayToString(title));
}

void wrap_taglib_tag_set_artist(TagLib_Tag *tag, const char *artist) {
  TagLib::Tag *t = reinterpret_cast<TagLib::Tag *>(tag);
  t->setArtist(charArrayToString(artist));
}

void wrap_taglib_tag_set_album(TagLib_Tag *tag, const char *album) {
  TagLib::Tag *t = reinterpret_cast<TagLib::Tag *>(tag);
  t->setAlbum(charArrayToString(album));
}

void wrap_taglib_opustag_add_field(TagLib_Tag *tag, const char *key, const char *value) {
  TagLib::Ogg::XiphComment *t = reinterpret_cast<TagLib::Ogg::XiphComment *>(tag);
  t->addField(charArrayToString(key), charArrayToString(value));
}

void wrap_taglib_opustag_remove_fields(TagLib_Tag *tag, const char *key) {
  TagLib::Ogg::XiphComment *t = reinterpret_cast<TagLib::Ogg::XiphComment *>(tag);
  t->removeFields(charArrayToString(key));
}

char *wrap_taglib_opustag_get_field(TagLib_Tag *tag, const char *key) {
  TagLib::Ogg::XiphComment *t = reinterpret_cast<TagLib::Ogg::XiphComment *>(tag);
  TagLib::Ogg::FieldListMap map = t->fieldListMap();
  if (map[charArrayToString(key)].isEmpty()) {
    return NULL;
  } else {
    auto first = map[charArrayToString(key)].front();
    return stringToCharArray(first);
  }
}