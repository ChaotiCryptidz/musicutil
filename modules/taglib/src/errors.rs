use thiserror::Error;

#[derive(Error, Debug)]
pub enum TagLibError {
	#[error("could not save file")]
	SaveError,
	#[error("invalid file")]
	InvalidFile,
	#[error("metadata unavailable")]
	MetadataUnavailable,
}
