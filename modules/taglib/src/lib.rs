#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
pub mod errors;
pub(crate) mod impls;
pub mod traits;
pub(crate) mod utils;

pub(crate) mod bindings {
	#![allow(non_snake_case)]
	#![allow(dead_code)]
	include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}

#[derive(Debug, Clone, Copy)]
pub enum TagLibFileType {
	OggFLAC = 1,
	OggOpus = 2,
	OggSpeex = 3,
	OggVorbis = 4,
	MP4 = 5,
}

pub use impls::file::*;
