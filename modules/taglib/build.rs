use std::env;
use std::path::PathBuf;

fn main() {
	println!("cargo:rerun-if-changed=src/wrapper.h");
	println!("cargo:rerun-if-changed=src/wrapper.cxx");

	let taglib = pkg_config::Config::new().probe("taglib").unwrap();

	let mut cc_builder = cc::Build::new();

	cc_builder
		.file("src/wrapper.cxx")
		.flag("-std=c++2b")
		.flag("-Og")
		.include("src")
		.cpp(true);

	for include in taglib.include_paths {
		cc_builder.include(include);
	}

	cc_builder.compile("wrapper");

	let bindings = bindgen::Builder::default()
		.header("src/wrapper.h")
		.generate()
		.expect("Unable to generate bindings");

	let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());

	bindings
		.write_to_file(out_path.join("bindings.rs"))
		.expect("Couldn't write bindings!");
}
