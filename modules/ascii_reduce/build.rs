use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::Path;

const MAPPINGS_DATA: &str = include_str!("src/mappings.json");

fn main() {
	let data: HashMap<String, String> =
		serde_json::from_str(MAPPINGS_DATA).expect("mapping data invalid");

	let mut replacement_map: HashMap<char, String> = HashMap::new();
	for (chr, repl) in &data {
		match chr.parse::<u32>() {
			Ok(n) => {
				let b = char::from_u32(n).expect("invalid char in string");

				replacement_map.insert(b, repl.to_string());
			}
			Err(e) => {
				panic!(
					"mapping data broken, could not parse char {} with error {}",
					chr, e
				);
			}
		}
	}

	let mut map: &mut phf_codegen::Map<char> = &mut phf_codegen::Map::new();
	for replacement in replacement_map.into_iter() {
		let r_char = replacement.1;
		map = map.entry(
			replacement.0,
			format!("\"{}\"", r_char.escape_debug()).as_str(),
		)
	}

	let path = Path::new(&env::var("OUT_DIR").unwrap()).join("codegen.rs");
	let mut file = BufWriter::new(File::create(path).unwrap());
	write!(
		&mut file,
		"static MAPPINGS: phf::Map<char, &'static str> = {}",
		map.build()
	)
	.unwrap();
	writeln!(&mut file, ";").unwrap();
}
