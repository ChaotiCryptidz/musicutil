include!(concat!(env!("OUT_DIR"), "/codegen.rs"));

#[cfg(test)]
mod tests {
	#[test]
	fn test() {
		assert_eq!(crate::reduce("öwo owö 😊".to_string()), "owo owo ");
	}
}

pub fn reduce(input: String) -> String {
	if input.is_ascii() {
		return input;
	}

	let mut output = String::with_capacity(input.len());
	for c in input.chars() {
		if c.is_ascii() {
			output.push(c);
			continue;
		}

		if let Some(replacement) = MAPPINGS.get(&c) {
			output.push_str(replacement);
		}
	}

	output
}
