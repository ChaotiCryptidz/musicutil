use std::{
	fmt,
	io::{BufRead, BufReader},
	path::PathBuf,
	process::{self, Command},
};

#[derive(Debug, Clone)]
pub struct ReplayGainRawData {
	pub track_gain: f64,
	pub track_peak: f64,
}

#[derive(Debug, Clone)]
pub struct ReplayGainData {
	pub track_gain: String,
	pub track_peak: String,
}

impl ReplayGainRawData {
	pub fn to_normal(&self, is_ogg_opus: bool) -> ReplayGainData {
		if is_ogg_opus {
			ReplayGainData {
				track_gain: format!("{:.6}", (self.track_gain * 256.0).ceil()),
				track_peak: "".to_string(), // Not Required
			}
		} else {
			ReplayGainData {
				track_gain: format!("{:.2} dB", self.track_gain),
				track_peak: format!("{:.6}", self.track_peak),
			}
		}
	}
}

#[derive(Debug, Clone)]
pub struct FFMpegError {
	pub exit_status: process::ExitStatus,
	pub stderr: String,
}

impl fmt::Display for FFMpegError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(
			f,
			"ffmpeg exited with error code {}, stderr: {}",
			self.exit_status.code().unwrap(),
			self.stderr
		)
	}
}

#[derive(Debug)]
pub enum Error {
	FFMpegError(FFMpegError),
	ParseError(std::num::ParseFloatError),
}

impl fmt::Display for Error {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Error::FFMpegError(err) => write!(f, "{}", err),
			Error::ParseError(err) => write!(f, "{}", err),
		}
	}
}

pub fn analyze_replaygain_track(
	path: PathBuf,
	ffmpeg_command: Option<&str>,
) -> Result<ReplayGainRawData, Error> {
	let output = Command::new(ffmpeg_command.unwrap_or("ffmpeg"))
		.args([
			"-hide_banner",
			"-nostats",
			"-v",
			"info",
			"-i",
			&path.to_string_lossy(),
			"-filter_complex",
			"[0:a]ebur128=framelog=verbose:peak=sample:dualmono=true[s6]",
			"-map",
			"[s6]",
			"-f",
			"null",
			"/dev/null",
		])
		.output();

	let output = output.unwrap();

	if !output.status.success() {
		return Err(Error::FFMpegError(FFMpegError {
			exit_status: output.status,
			stderr: String::from_utf8(output.stderr).unwrap(),
		}));
	}
	// info we need is in stdout
	let output_str = String::from_utf8(output.stderr).unwrap();

	let mut ebur128_summary = String::new();

	// for some reason ffmpeg outputs the summary twice,
	// the first time is garbage data
	let mut has_seen_first_summary = false;
	let mut should_start_reading_lines = false;

	let output_reader = BufReader::new(output_str.as_bytes());
	for line in output_reader.lines() {
		if let Ok(line) = line {
			if line.starts_with("[Parsed_ebur128_0") {
				if has_seen_first_summary {
					should_start_reading_lines = true;
					ebur128_summary.push_str("Summary:\n")
				} else {
					has_seen_first_summary = true;
				}
			} else if should_start_reading_lines {
				ebur128_summary.push_str(line.trim());
				ebur128_summary.push('\n')
			}
		} else {
			break;
		}
	}

	let mut track_gain: f64 = 0.0;
	let mut track_peak: f64 = 0.0;

	let summary_reader = BufReader::new(ebur128_summary.as_bytes());
	for line in summary_reader.lines() {
		if let Ok(line) = line {
			if line.starts_with("I:") {
				let mut l = line.split(':');
				l.next();

				let gain = l.next().unwrap().trim().trim_end_matches(" LUFS");
				let gain = match gain.parse::<f64>() {
					Ok(parsed) => Ok(parsed),
					Err(err) => Err(Error::ParseError(err)),
				}?;

				// https://wiki.hydrogenaud.io/index.php?title=ReplayGain_2.0_specification#Gain_calculation
				// "In order to maintain backwards compatibility with RG1, RG2 uses a -18 LUFS reference, which based on lots of music, can give similar loudness compared to RG1."
				let gain = -18_f64 - gain;

				track_gain = gain;
			}

			if line.starts_with("Peak:") {
				let mut l = line.split(':');
				l.next();

				// https://wiki.hydrogenaud.io/index.php?title=ReplayGain_2.0_specification#Loudness_normalization
				let peak = l.next().unwrap().trim().trim_end_matches(" dBFS");
				let peak = match peak.parse::<f64>() {
					Ok(parsed) => Ok(parsed),
					Err(err) => Err(Error::ParseError(err)),
				}?;
				let peak = f64::powf(10_f64, peak / 20.0_f64);
				track_peak = peak;
			}
		} else {
			break;
		}
	}
	Ok(ReplayGainRawData {
		track_gain,
		track_peak,
	})
}
